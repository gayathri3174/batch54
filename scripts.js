let userTask = document.getElementById('userTask');
let task = document.getElementById('task');
let Errormsg = document.getElementById('errormsg')
let addbtn = document.getElementById('addbtn');
let unorderlst = document.getElementById('tasklistcont')
let Tasks = []
addbtn.addEventListener('click',()=>{
    if(task.value.trim() ==""){
      Errormsg.innerHTML = "Please Enter the task";
      Errormsg.style.color='red';
      Errormsg.style.fontSize='10px';
    }else{
        Errormsg.innerHTML ="";
        Tasks.push(task.value);
        console.log(Tasks);
        task.value = "";
        unorderlst.innerHTML = "";
        displaytasks();
    

    }
   
})


function displaytasks(){
    Tasks.forEach((task,index)=>{
        let lstele = document.createElement('li');
        lstele.innerHTML = task;
        lstele.classList.add('lststyle');
        lstele.onclick = ()=>{
               
            lstele.classList.toggle('completedtask');
         
        }
        let icon = document.createElement('i');
        icon.classList.add('fa-solid','fa-delete-left');
        icon.onclick = () => {
            Tasks.splice(index, 1);
            console.log(Tasks)
            unorderlst.innerHTML = ""; 
            displaytasks(); 
        };
       
        let divcontainer = document.createElement('div');
        divcontainer.classList.add('usertask');
        divcontainer.appendChild(lstele);
        divcontainer.appendChild(icon);
        unorderlst.appendChild(divcontainer);


    })
}



